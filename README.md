# NuttX firmware for Kospet DK08 smartwatch

This firmware allows to replace stock firmware for DK08 watch
with NuttX. The functionality is quite basic, it mostly works
as a watch/alarm-clock. Also, BLE is not currently supported since
nRF52 BLE support is a work in progress in NuttX.

**NOTE**: this code is mostly a starting point for someone willing
to play with the watch. I'm not after a big project to provide complete
functionality so I will not be adding more features. Therefore, please
do not request features or open pull-requests. Also, note that I cannot
provide support for the code. Also, there's a risk in bricking the watch,
so be warned.

## Status

The current firmware makes good use of low power features of nRF52
and of the transflective LCD. The display is only updated at the top of
every minute and the display is set to low refresh rate mode while
it is not being used.

The interface is menu based and quite simple. In general, holding the
touch button makes an action and a short press allows navigation between
options (or values of a widget, such as when setting time).

On the main clock screen, touching the button once will illuminate the display
for a brief period of time. A long press goes into menu where you can set time,
date and alarm.

As a safety feature against bricking, there's a 10 second watchdog (enabled
by the bootloader) which is paused during sleep. While the button is pressed,
the watchdog is not fed, so if you hold the button for 10s it will reboot into
DFU mode. You can then reflash if you need to or if you just wait it will eventually timeout and boot current image again. Note that whenever you reboot
settings will be lost.

Finally, you will see the battery voltage displayed on top. At the left, two numbers (either 0 or 1 will be shown): one indicates if it is being charged and
the other if the charge is complete. I have not been able to get charge complete
notification via this pin though. Also, I'm currently guessing the battery measurement voltage ratio so it may not be entirely accurate.

## Installation

You will need a customized bootloader which enables watchdog and a few other minor things. The safest approach is to:

1. Install ESPruino project bootloader
2. Install my customized bootloader
3. Install this firmware

You can revert to the original firmware at any time.

For the first two steps, follow the instructions [here](https://gitlab.com/nuttx_projects/dk08/nrf52-legacy-bootloader). Then, to build this firmware do:

    $ git submodule update --init
    $ cd nuttx/
    $ tools/configure.sh -l ../kospet-dk08/configs/watch
    $ make
    $ cd ..
    $ scripts/pkg.sh

Now, reboot into bootloader and do:

    $ scripts/flash-ota.sh

   
