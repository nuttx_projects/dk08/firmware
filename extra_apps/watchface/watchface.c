/****************************************************************************
 * %{FileName}
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdint.h>

#define WATCHFACE_HDR         "HMDIAL"
#define WATCHFACE_HDRLEN      (6)

#define PARAM_HAS_CHILDREN    (2)

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/* Amazfit Bip format */

begin_packed_struct struct wf_bin_hdr_s
{
  uint8_t magic[6];
  uint8_t version;
  uint8_t unknown[9];

  uint8_t unknown2[20];
  uint32_t parameter_size;
} end_packed_struct;

begin_packed_struct struct wf_bin_param_s
{
  uint8_t id;
  uint32_t value;
} end_packed_struct;

int wf_read_param(int fd, struct wf_bin_param_s *param)
{
  int i = 0;
  int offset = 0;

  uint8_t raw_id;

  /* read ID */

  if (read(fd, &raw_id, 1) != 1)
    {
      perror("read ID");
      return ERROR;
    }

  param->id = (raw_id & 0xf8) >> 3;

  /* read value */

  param->value = 0;

  uint8_t data;
  do
    {
      if (read(fd, &data, 1) != 1)
        {
          perror("read value");
          return ERROR;
        }

      param->value |= ((data & 0x7F) << offset);
      offset += 7;

    } while ((i < 4) && data & 0x80);

  if (data & 0x80 && i == 4)
    {
      /* Invalid length */

      return 1;
    }

  return OK;
}

/****************************************************************************
 * wf_open
 ****************************************************************************/

int wf_open(const char* path)
{
  int fd;
  int ret = OK;
  char buf[32];
  struct wf_bin_param_s main_param, table_length_param, image_count_param;

  /* Open file */

  fd = open(path, O_RDONLY);

  if (fd < 0)
    {
      perror("open watchface");
      return ERROR;
    }

  /* Check header */

  ret = read(fd, buf, WATCHFACE_HDR_LEN);

  if (ret != WATCHFACE_HDRLEN ||
      strncmp(WATCHFACE_HDR, buf, WATCHFACE_HDRLEN) != 0)
    {
      fprintf(stderr, "Bad file header\n");
      ret = -EINVAL;
      goto out;
    }

  /* Get main parameter */

  wf_read_param(fd, &main_param);
  wf_read_param(fd, &table_length_param);
  wf_read_param(fd, &image_count_param);

  return 0;
}






out:
  close(fd);
  return ret;
}

