/****************************************************************************
 * boards/arm/nrf52/kospet-dk08/include/board.h
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __BOARDS_ARM_NRF52_KOSPET_DK08_INCLUDE_BOARD_H
#define __BOARDS_ARM_NRF52_KOSPET_DK08_INCLUDE_BOARD_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdbool.h>
#include <sys/boardctl.h>

#if defined(CONFIG_ARCH_IRQBUTTONS) && defined(CONFIG_NRF52_GPIOTE)
#  include <nuttx/irq.h>
#endif

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Reset types **************************************************************/

#define BOARD_RESET_NORMAL    0
#define BOARD_RESET_DFU       1
#define BOARD_RESET_CRASH     2

/* Boardctl commands ********************************************************/

#define BOARDIOC_MOTOR      (BOARDIOC_USER + 0)  /* int: 1 (on), 0 (off) */
#define BOARDIOC_BACKLIGHT  (BOARDIOC_USER + 1)  /* int: 1 (on), 0 (off) */
#define BOARDIOC_CHARGING   (BOARDIOC_USER + 2)  /* bool*: 1 (charging), 0 (not charging) */
#define BOARDIOC_CHARGED    (BOARDIOC_USER + 3)  /* bool*: 1 (charged), 0 (not charged) */
#define BOARDIOC_INFO       (BOARDIOC_USER + 4)  /* nrf52_device_info_s* */

/* Clocking *****************************************************************/

#define BOARD_SYSTICK_CLOCK         (64000000)

/* Button definitions *******************************************************/

#define BOARD_BUTTONS 1

#define BUTTON_TOUCH  0

/* UART Pins ****************************************************************/

#if 0
#define BOARD_UART0_RX_PIN  (GPIO_INPUT  | GPIO_PORT0 | GPIO_PIN(19))
#define BOARD_UART0_TX_PIN  (GPIO_OUTPUT | GPIO_PORT0 | GPIO_PIN(20))
#endif

/* SPI Pins *****************************************************************/

/* LCD SPI */

#define BOARD_SPI0_SCK_PIN  (GPIO_OUTPUT | GPIO_PORT0 | GPIO_PIN(17))
#define BOARD_SPI0_MOSI_PIN (GPIO_OUTPUT | GPIO_PORT0 | GPIO_PIN(16))

/* Flash and accelerometer SPI */

#define BOARD_SPI1_SCK_PIN  (GPIO_OUTPUT | GPIO_PORT0 | GPIO_PIN(29))
#define BOARD_SPI1_MOSI_PIN (GPIO_OUTPUT | GPIO_PORT0 | GPIO_PIN(30))
#define BOARD_SPI1_MISO_PIN (GPIO_INPUT | GPIO_PORT0 | GPIO_PIN(31))

/* Other GPIOs **************************************************************/

#define GPIO_BACKLIGHT     (GPIO_OUTPUT | GPIO_PORT0 | GPIO_VALUE_ZERO | GPIO_PIN(13))
#define NRF52_PWM0_CH0_PIN  GPIO_BACKLIGHT

#define GPIO_MOTOR     (GPIO_OUTPUT | GPIO_PORT0 | GPIO_VALUE_ZERO | GPIO_PIN(6))
#define NRF52_PWM1_CH0_PIN  GPIO_MOTOR

#define GPIO_POWER     (GPIO_INPUT  | GPIO_PORT0 | GPIO_PIN(24))
#define GPIO_CHARGED   (GPIO_INPUT  | GPIO_PORT0 | GPIO_PULLUP | GPIO_PIN(23))

#define GPIO_HR_POWER  (GPIO_OUTPUT | GPIO_PORT0 | GPIO_VALUE_ZERO | GPIO_PIN(14))

#define GPIO_ACC_POWER  (GPIO_OUTPUT | GPIO_PORT0 | GPIO_VALUE_ZERO | GPIO_PIN(7))

struct nrf52_device_info_s
{
  uint32_t part;
  uint32_t variant;
  uint32_t package;
  uint32_t ram;
  uint32_t flash;
};

#endif /* __BOARDS_ARM_NRF52_KOSPET_DK08_INCLUDE_BOARD_H */
