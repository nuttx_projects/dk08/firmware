/****************************************************************************
 * boards/arm/nrf52/kospet-dk08/src/kospet-dk08.h
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __BOARDS_ARM_NRF52_KOSPET_DK08_SRC_KOSPET_DK08_H
#define __BOARDS_ARM_NRF52_KOSPET_DK08_SRC_KOSPET_DK08_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/compiler.h>
#include <nuttx/spi/spi.h>

#include "nrf52_gpio.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Button definitions *******************************************************/

/* SENSE_HIGH is critical to wakeup from sleep with the button and resume
 * the watchdog. Otherwise we may go into sleep forever (while watchdog is
 * paused).
 */

#define GPIO_BTN_TOUCH  (GPIO_INPUT | GPIO_SENSE_HIGH | GPIO_PULLDOWN | GPIO_PIN(8))

/* DEBUG! do not use on final board */

//#define GPIO_DEBUG  (GPIO_OUTPUT | GPIO_VALUE_ZERO | GPIO_PIN(12))

/****************************************************************************
 * Public Types
 ****************************************************************************/

/****************************************************************************
 * Public data
 ****************************************************************************/

#ifndef __ASSEMBLY__

extern struct spi_dev_s* g_spi0;

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Name: nrf52_bringup
 *
 * Description:
 *   Perform architecture-specific initialization
 *
 *   CONFIG_BOARD_LATE_INITIALIZE=y :
 *     Called from board_late_initialize().
 *
 *   CONFIG_BOARD_LATE_INITIALIZE=n && CONFIG_LIB_BOARDCTL=y :
 *     Called from the NSH library
 *
 ****************************************************************************/

int nrf52_bringup(void);

/****************************************************************************
 * Name: nrf52_spidev_initialize
 ****************************************************************************/

void nrf52_spidev_initialize(void);

#endif /* __ASSEMBLY__ */
#endif /* __BOARDS_ARM_NRF52_KOSPET_DK08_SRC_KOSPET_DK08_H */
