/****************************************************************************
 * /home/v01d/coding/nuttx_dk08/kospet-dk08/src/nrf52_boardctl.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <arch/board/board.h>
#include <errno.h>
#include "arm_arch.h"
#include "nrf52_gpio.h"
#include "hardware/nrf52_ficr.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

int board_ioctl(unsigned int cmd, uintptr_t arg)
{
  int ret = 0;

  switch(cmd)
    {
#if !defined(CONFIG_NRF52_PWM0) && !defined(CONFIG_PWM)
      case BOARDIOC_BACKLIGHT:
        {
          nrf52_gpio_write(GPIO_BACKLIGHT, (bool)arg);
        }
        break;
#endif
#if !defined(CONFIG_NRF52_PWM1) && !defined(CONFIG_PWM)
      case BOARDIOC_MOTOR:
        {
          nrf52_gpio_write(GPIO_MOTOR, (bool)arg);
        }
        break;
#endif
      case BOARDIOC_CHARGED:
        {
          DEBUGASSERT(arg);
          *(bool*)arg = nrf52_gpio_read(GPIO_CHARGED);
          break;
        }
      case BOARDIOC_CHARGING:
        {
          DEBUGASSERT(arg);
          *(bool*)arg = nrf52_gpio_read(GPIO_POWER);
          break;
        }
      case BOARDIOC_INFO:
        {
          DEBUGASSERT(arg);
          struct nrf52_device_info_s *info =
              (struct nrf52_device_info_s*)arg;

          info->part = getreg32(NRF52_FICR_INFO_PART);
          info->variant = getreg32(NRF52_FICR_INFO_VARIANT);
          info->package = getreg32(NRF52_FICR_INFO_PACKAGE);
          info->ram = getreg32(NRF52_FICR_INFO_RAM);
          info->flash = getreg32(NRF52_FICR_INFO_FLASH);
          break;
        }
      default:
        ret = -ENOSYS;
        break;
    }

  return ret;
}

