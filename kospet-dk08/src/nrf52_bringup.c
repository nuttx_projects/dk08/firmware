/****************************************************************************
 * boards/arm/nrf52/kospet-dk08/src/nrf52_bringup.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <syslog.h>
#include <nuttx/board.h>
#include <arch/board/board.h>

#include "kospet-dk08.h"
#include "arm_arch.h"

#ifdef CONFIG_NRF52_WDT
#  include "nrf52_wdt_lowerhalf.h"
#endif

#ifdef CONFIG_USERLED
#  include <nuttx/leds/userled.h>
#endif

#ifdef CONFIG_BUTTONS
#include <nuttx/input/buttons.h>
#endif

#ifdef CONFIG_LCD
#include "nrf52_st7301.h"
#include "nrf52_spi.h"

#ifdef CONFIG_LCD_DEV
#include <nuttx/lcd/lcd_dev.h>
#endif
#endif

#if defined(CONFIG_NRF52_SAADC) && defined(CONFIG_ADC)
#include <nuttx/analog/adc.h>
#include "nrf52_adc.h"

const static struct nrf52_adc_channel_s g_adc_chan =
{
#if 1
  .p_psel = NRF52_ADC_IN_IN3,
#else
  .p_psel = NRF52_ADC_IN_VDD,
#endif
  .n_psel = NRF52_ADC_IN_NC,
  .resn = NRF52_ADC_RES_BYPASS,
  .resp = NRF52_ADC_RES_BYPASS,
  .mode = NRF52_ADC_MODE_SE,
#if 1
  .refsel = NRF52_ADC_REFSEL_VDD_4,
  .gain = NRF52_ADC_GAIN_1_4,
#else
  .refsel = NRF52_ADC_REFSEL_INTERNAL,
  .gain = NRF52_ADC_GAIN_1_6,
#endif
  .tacq = NRF52_ADC_TACQ_40US,
  .burst = NRF52_ADC_BURST_DISABLE,
};
#endif

#if defined(CONFIG_NRF52_PWM) && defined(CONFIG_PWM)
#include <nuttx/timers/pwm.h>
#include "nrf52_pwm.h"
#endif

#include "hardware/nrf52_radio.h"
#include "hardware/nrf52_twi.h"

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: nrf52_bringup
 *
 * Description:
 *   Perform architecture-specific initialization
 *
 *   CONFIG_BOARD_LATE_INITIALIZE=y :
 *     Called from board_late_initialize().
 *
 *   CONFIG_BOARD_LATE_INITIALIZE=n && CONFIG_LIB_BOARDCTL=y :
 *     Called from the NSH library
 *
 ****************************************************************************/

int nrf52_bringup(void)
{
  int ret = OK;

  /* Initialize some GPIOs */

#if !defined(CONFIG_NRF52_PWM0) && !defined(CONFIG_PWM)
  nrf52_gpio_config(GPIO_BACKLIGHT);
#endif

#if !defined(CONFIG_NRF52_PWM1) && !defined(CONFIG_PWM)
  nrf52_gpio_config(GPIO_MOTOR);
#endif

#ifdef GPIO_DEBUG
  nrf52_gpio_config(GPIO_DEBUG);
#endif

  nrf52_gpio_config(GPIO_POWER);
  nrf52_gpio_config(GPIO_CHARGED);
  nrf52_gpio_config(GPIO_HR_POWER);
  nrf52_gpio_config(GPIO_ACC_POWER);

#ifdef CONFIG_WATCHDOG
  /* Start Watchdog timer */

  ret = nrf52_wdt_initialize(CONFIG_WATCHDOG_DEVPATH, 1, 1);
  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: nrf52_wdt_initialize failed: %d\n", ret);
    }
#endif

#ifdef CONFIG_BUTTONS
  ret = btn_lower_initialize("/dev/buttons");

  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: btn_lower_initialize() failed: %d\n", ret);
    }
#endif

#ifdef CONFIG_USERLED
  /* Register the LED driver */

  ret = userled_lower_initialize(CONFIG_EXAMPLES_LEDS_DEVPATH);
  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: userled_lower_initialize() failed: %d\n", ret);
    }
#endif

  /* Do some bootloader cleanup */

  putreg32(0, NRF52_RADIO_POWER);

  /* Initialize SPI buses */

  nrf52_spidev_initialize();

#ifdef CONFIG_LCD
  /* Initialize LCD */

  ret = board_lcd_initialize();

  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: board_lcd_initialize() failed: %d\n", ret);
    }

#ifdef CONFIG_LCD_DEV
  /* Register LCD character device */

  ret = lcddev_register(0);

  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: lcddev_register() failed: %d\n", ret);
    }
#endif
#endif

#if defined(CONFIG_NRF52_SAADC) && defined(CONFIG_ADC)
  ret = adc_register("/dev/battery", nrf52_adcinitialize(&g_adc_chan, 1));

  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: adc_register() failed: %d\n", ret);
    }
#endif

#if defined(CONFIG_NRF52_PWM0) && defined(CONFIG_PWM)
  ret = pwm_register("/dev/backlight", nrf52_pwminitialize(0));

  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: pwm_register() failed: %d\n", ret);
    }
#endif

#if defined(CONFIG_NRF52_PWM1) && defined(CONFIG_PWM)
  ret = pwm_register("/dev/motor", nrf52_pwminitialize(1));

  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: pwm_register() failed: %d\n", ret);
    }
#endif
  return ret;
}
