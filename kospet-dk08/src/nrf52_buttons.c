/****************************************************************************
 * boards/arm/nrf52/kospet-dk08/src/nrf52_buttons.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stddef.h>
#include <errno.h>

#include <nuttx/irq.h>
#include <nuttx/board.h>

#include <arch/board/board.h>

#include "nrf52_gpio.h"
#include "nrf52_gpiote.h"
#include "kospet-dk08.h"

int empty_isr(int irq, FAR void *context, FAR void *arg)
{
  return 0;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: board_button_initialize
 *
 * Description:
 *   board_button_initialize() must be called to initialize button resources.
 *   After that, board_buttons() may be called to collect the current state
 *   of all buttons or board_button_irq() may be called to register button
 *   interrupt handlers.
 *
 ****************************************************************************/

uint32_t board_button_initialize(void)
{
  /* This will setup the touch button, it will already have been set
   * by the bootloader to SENSE_HIGH so that we always can wake up from
   * sleep with it. The config here should not be needed but we do it again
   * anyway
   */

  nrf52_gpio_config(GPIO_BTN_TOUCH);

  /* We also register an empty interrupt for the SENSE action, this ensure
   * WFI will be exited even in the case where WFI is entered with the
   * button pressed (which will NOT generate a new DETECT signal)
   */

  nrf52_gpiote_set_pin_event(GPIO_BTN_TOUCH, empty_isr, NULL);

  return BOARD_BUTTONS;
}

/****************************************************************************
 * Name: board_buttons
 ****************************************************************************/

uint32_t board_buttons(void)
{
  return nrf52_gpio_read(GPIO_BTN_TOUCH) ? 1 : 0;
}

/****************************************************************************
 * Button support.
 *
 * Description:
 *   board_button_initialize() must be called to initialize button resources.
 *   After that, board_buttons() may be called to collect the current
 *   state of all buttons or board_button_irq() may be called to register
 *   button interrupt handlers.
 *
 *   After board_button_initialize() has been called, board_buttons()
 *   may be called to collect the state of all buttons.  board_buttons()
 *   returns an 32-bit bit set with each bit associated with a button.
 *   See the BUTTON_*_BIT definitions in board.h for the meaning of each
 *   bit.
 *
 *   board_button_irq() may be called to register an interrupt handler that
 *    will be called when a button is depressed or released.  The ID value
 *   is a button enumeration value that uniquely identifies a button
 *   resource. See the BUTTON_* definitions in board.h for the meaning of
 *   enumeration value.
 *
 ****************************************************************************/

#ifdef CONFIG_ARCH_IRQBUTTONS
int board_button_irq(int id, xcpt_t irqhandler, FAR void *arg)
{
  int ret = -EINVAL;

  if (id == BUTTON_TOUCH)
    {
      nrf52_gpiote_set_ch_event(GPIO_BTN_TOUCH, 0, true, true,
                                irqhandler, arg);
    }

  return ret;
}
#endif
