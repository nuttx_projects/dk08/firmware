/****************************************************************************
 * /home/v01d/coding/nuttx_dk08/kospet-dk08/src/nrf52_lcd.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/arch.h>
#include <syslog.h>
#include "kospet-dk08.h"
#include "nrf52_spi.h"
#include "nrf52_gpio.h"
#include "nrf52_st7301.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define GPIO_LCD_RST     (GPIO_OUTPUT | GPIO_VALUE_ZERO | GPIO_PORT0 | GPIO_PIN(15))
#define GPIO_LCD_PWR     (GPIO_OUTPUT | GPIO_VALUE_ZERO | GPIO_PORT0 | GPIO_PIN(11))

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

struct lcd_dev_s *g_lcddev = NULL;

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

int board_lcd_initialize(void)
{
  /* Turn LCD power ON */

  nrf52_gpio_config(GPIO_LCD_PWR); /* active-low, start enabled */

  /* Reset LCD */

  nrf52_gpio_config(GPIO_LCD_RST); /* active-low, start asserted */
  up_mdelay(2);
  nrf52_gpio_write(GPIO_LCD_RST, true); /* deassert */
  up_mdelay(120);

  /* Initialize LCD */

  g_lcddev = st7301_lcdinitialize(g_spi0);

  if (!g_lcddev)
    {
      syslog(LOG_ERR, "Could not initialize LCD\n");
      return -ENODEV;
    }

  return 0;
}

FAR struct lcd_dev_s *board_lcd_getdev(int lcddev)
{
  DEBUGASSERT(lcddev == 0);

  return g_lcddev;
}

void board_lcd_uninitialize(void)
{
  /* TODO */
}
