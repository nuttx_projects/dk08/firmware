/****************************************************************************
 * /home/v01d/coding/nuttx_dk08/kospet-dk08/src/nrf52_reset.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/arch.h>
#include <arch/board/board.h>
#include "hardware/nrf52_power.h"
#include "arm_arch.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

int board_reset(int cmd)
{
  switch(cmd)
    {
      case BOARD_RESET_NORMAL:

        /* Do nothing else, will just boot again after passing into
         * bootloader
         */

        break;

      case BOARD_RESET_DFU:

        /* Request entry to DFU mode by setting special register */

        putreg32(1, NRF52_POWER_GPREGRET);

        break;

      case BOARD_RESET_CRASH:

        /* System has crashed, also request entry to DFU, but do
         * something else later (TODO)
         */

        putreg32(1, NRF52_POWER_GPREGRET);

        break;
    }

  up_systemreset();
  return 0;
}

