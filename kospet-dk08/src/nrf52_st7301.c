/****************************************************************************
 * drivers/lcd/st7301.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/arch.h>
#include <nuttx/spi/spi.h>
#include <nuttx/lcd/lcd.h>
#include "nrf52_gpio.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* SPI commands */

#define CMD_SWRESET     0x01   /* Software reset */
#define CMD_RDDID       0x04   /* Read display ID */
#define CMD_RDDST       0x09   /* Read display status */
#define CMD_RDDPM       0x0a   /* Read display power mode */
#define CMD_SLPIN       0x10   /* Sleep in */
#define CMD_SLPOUT      0x11   /* Sleep out */
#define CMD_PTLON       0x12   /* Partial On */
#define CMD_PTLOFF      0x13   /* Partial Off */
#define CMD_INVOFF      0x20   /* Display inversion OFF */
#define CMD_INVON       0x21   /* Display inversion ON */
#define CMD_DISPOFF     0x28   /* Display OFF */
#define CMD_DISPON      0x29   /* Display ON */
#define CMD_CASET       0x2a   /* Column Address Set */
#define CMD_RASET       0x2b   /* Row Address Set */
#define CMD_RAMWR       0x2c   /* Memory Write */
#define CMD_RAMRD       0x2e   /* Memory Read */
#define CMD_TEOFF       0x34   /* Tearing Effect Line Off */
#define CMD_TEON        0x35   /* Tearing Effect Line On */
#define CMD_MADCTL      0x36   /* Memory Data Access Control */
#define CMD_VSCSAD      0x37   /* Vertical Scroll Start Address of RAM */
#define CMD_HPM         0x38   /* High Power Mode ON */
#define CMD_LPM         0x39   /* Low Power Mode ON */
#define CMD_DTFORM      0x3a   /* Data Format Select */
#define CMD_WRMEMC      0x3c   /* Write Memory Continue */
#define CMD_RDMEMC      0x3e   /* Read Memory Continue */
#define CMD_TESCAN      0x44   /* Set Tear Scanline */
#define CMD_RDID1       0xda   /* Read ID1 */
#define CMD_RDID2       0xdb   /* Read ID2 */
#define CMD_RDID3       0xdc   /* Read ID3 */

#define CMD_GPS         0x72   /* Gate Power Save */
#define CMD_GATESET     0xb0   /* Gate Line Setting */
#define CMD_FSTCOM      0xb1   /* First Gate Setting */
#define CMD_FRCTRL      0xb2   /* Frame Rate Control */
#define CMD_VCOMEQ      0xb3   /* VCOM EQ Enable */
#define CMD_GATEUPDEQ   0xb4   /* Update Period Gate EQ Control */
#define CMD_SOUEQ       0xb7   /* Source EQ Enable */
#define CMD_PNLSET      0xb8   /* Panel Setting */
#define CMD_SOCSET      0xb9   /* Source Setting */
#define CMD_CLRAM       0xba   /* Enable Cler RAM */
#define CMD_GCTRL       0xc0   /* Gate Voltage Control */
#define CMD_SCTRL1      0xc1   /* Source Voltage Control 1 */
#define CMD_SCTRL2      0xc2   /* Source Voltage Control 2 */
#define CMD_OSCSET      0xc7   /* OSC Setting */
#define CMD_VCOMCTRL    0xcb   /* VCOMH Voltage Setting */
#define CMD_ID1SET      0xcc   /* ID1 Setting */
#define CMD_ID2SET      0xcd   /* ID2 Setting */
#define CMD_ID3SET      0xce   /* ID3 Setting */
#define CMD_AUTOPWRCTRL 0xd0   /* Auto Power Control */
#define CMD_BSTEN       0xd1   /* Booster Enable */
#define CMD_VSHLSEL     0xd6   /* Source Voltage Select */
#define CMD_NVMLOADCTRL 0xd7   /* NVM Load Control */
#define CMD_HIGHVMODE   0xd8   /* High Voltage Mode */
#define CMD_DBSPISET    0xe4   /* 4SPI Input Data Select */
#define CMD_NVMRD       0xe9   /* NVM Data Read */
#define CMD_NVMLOADEN   0xeb   /* NVM Load Enable */
#define CMD_EXTBCTRL    0xec   /* EXTB Control */
#define CMD_NVMCTRL1    0xf8   /* NVM WR/RD Control */
#define CMD_NVMCTRL2    0xfa   /* NVM Program Setting */
#define CMD_NVMRDEN     0xfb   /* NVM Read Enable */
#define CMD_NVMPROM     0xfc   /* NVM Program Enable */

/* Bit fields */

#define DTFORM_BPS_SHIFT    0
#define DTFORM_BPS_3WRITE   (1 << DTFORM_BPS_SHIFT)
#define DTFORM_XDE_SHIFT    4
#define DTFORM_XDE_ON       (1 << DTFORM_XDE_SHIFT)
#define DTFORM_M8C_SHIFT    5
#define DTFORM_M8C_8COLOR   (1 << DTFORM_M8C_SHIFT)

#define MADCTL_GS_SHIFT     2
#define MADCTL_GS_BOT_TOP   (1 << MADCTL_GS_SHIFT) /* Bottom to top */
#define MADCTL_DO_SHIFT     3
#define MADCTL_DO_RTL       (1 << MADCTL_DO_SHIFT) /* Right-to-left (with MX=1) */
#define MADCTL_MV_SHIFT     5
#define MADCTL_MV_PAGE_DIR  (1 << MADCTL_MV_SHIFT) /* Page direction mode */
#define MADCTL_MX_SHIFT     6
#define MADCTL_MX_RTL       (1 << MADCTL_MX_SHIFT) /* Right-to-left */
#define MADCTL_MY_SHIFT     7
#define MADCTL_MY_BOT_TOP   (1 << MADCTL_MY_SHIFT) /* Bottom-to-top */

/* We need the column range to be a multiple of four (this assumes
 * four byte writes)
 */

#define VALID_COL_RANGE(s,e)  (((s) & 0b11) == 0 && ((e) & 0b11) == 0b11)

/* TODO: check below */

/* Verify that all configuration requirements have been met */

#ifndef CONFIG_LCD_ST7301_SPIMODE
#  define CONFIG_LCD_ST7301_SPIMODE SPIDEV_MODE0
#endif

/* SPI frequency */

#ifndef CONFIG_LCD_ST7301_FREQUENCY
#  define CONFIG_LCD_ST7301_FREQUENCY 8000000
#endif

/* Check contrast selection */

#if !defined(CONFIG_LCD_MAXCONTRAST)
#  define CONFIG_LCD_MAXCONTRAST 1
#endif

/* Check power setting */

#if !defined(CONFIG_LCD_MAXPOWER) || CONFIG_LCD_MAXPOWER < 1
#  define CONFIG_LCD_MAXPOWER 1
#endif

#if CONFIG_LCD_MAXPOWER > 255
#  error "CONFIG_LCD_MAXPOWER must be less than 256 to fit in uint8_t"
#endif

/* Check orientation */

#if defined(CONFIG_LCD_PORTRAIT)
#  if defined(CONFIG_LCD_LANDSCAPE) || defined(CONFIG_LCD_RLANDSCAPE) ||\
      defined(CONFIG_LCD_RPORTRAIT)
#    error "Cannot define both portrait and any other orientations"
#  endif
#elif defined(CONFIG_LCD_RPORTRAIT)
#  if defined(CONFIG_LCD_LANDSCAPE) || defined(CONFIG_LCD_RLANDSCAPE)
#    error "Cannot define both rportrait and any other orientations"
#  endif
#elif defined(CONFIG_LCD_LANDSCAPE)
#  ifdef CONFIG_LCD_RLANDSCAPE
#    error "Cannot define both landscape and any other orientations"
#  endif
#elif !defined(CONFIG_LCD_RLANDSCAPE)
#  define CONFIG_LCD_LANDSCAPE 1
#endif

/* Display Resolution */

#define CONFIG_ST7301_XRES 176
#define CONFIG_ST7301_YRES 176
#define ST7301_LUT_SIZE    176

#define ST7301_XRES        176
#define ST7301_YRES        176
#define ST7301_COLORFMT    FB_FMT_RGB8_222

#define ST7301_BPP         8

/****************************************************************************
 * Private Types
 ****************************************************************************/

/* This structure describes the state of this driver */

struct st7301_dev_s
{
  /* Publicly visible device structure */

  struct lcd_dev_s dev;

  /* Private LCD-specific information follows */

  FAR struct spi_dev_s *spi;  /* SPI device */
  uint8_t bpp;                /* Selected color depth */
  uint8_t power;              /* Current power setting */
};

/****************************************************************************
 * Private Function Protototypes
 ****************************************************************************/

/* Misc. Helpers */

static void st7301_select(FAR struct spi_dev_s *spi, int bits);
static void st7301_deselect(FAR struct spi_dev_s *spi);

static inline void st7301_sendcmd(FAR struct st7301_dev_s *dev, uint8_t cmd);
static void st7301_sleep(FAR struct st7301_dev_s *dev, bool sleep);
static void st7301_display(FAR struct st7301_dev_s *dev, bool on);
static void st7301_setarea(FAR struct st7301_dev_s *dev,
                           uint16_t x0, uint16_t y0,
                           uint16_t x1, uint16_t y1);
static void st7301_wrram(FAR struct st7301_dev_s *dev,
                         FAR const uint16_t *buff, size_t size);

/* LCD Data Transfer Methods */

static int st7301_putrun(fb_coord_t row, fb_coord_t col,
                         FAR const uint8_t *buffer, size_t npixels);

static int st7301_putarea(fb_coord_t row_start, fb_coord_t row_end,
                          fb_coord_t col_start, fb_coord_t col_end,
                          FAR const uint8_t *buffer);

/* LCD Configuration */

static int st7301_getvideoinfo(FAR struct lcd_dev_s *dev,
                               FAR struct fb_videoinfo_s *vinfo);
static int st7301_getplaneinfo(FAR struct lcd_dev_s *dev,
                               unsigned int planeno,
                               FAR struct lcd_planeinfo_s *pinfo);

/* LCD Specific Controls */

static int st7301_getpower(FAR struct lcd_dev_s *dev);
static int st7301_setpower(FAR struct lcd_dev_s *dev, int power);
static int st7301_getcontrast(FAR struct lcd_dev_s *dev);
static int st7301_setcontrast(FAR struct lcd_dev_s *dev,
                              unsigned int contrast);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static struct st7301_dev_s g_lcddev =
{
  .dev = {},
  .bpp = ST7301_BPP
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: st7301_select
 *
 * Description:
 *   Select the SPI, locking and  re-configuring if necessary
 *
 * Input Parameters:
 *   spi   - Reference to the SPI driver structure
 *   bits  - Number of SPI bits
 *
 * Returned Value:
 *   None
 *
 * Assumptions:
 *
 ****************************************************************************/

static void st7301_select(FAR struct spi_dev_s *spi, int bits)
{
  /* Select st7301 chip (locking the SPI bus in case there are multiple
   * devices competing for the SPI bus
   */

  SPI_LOCK(spi, true);
  SPI_SELECT(spi, SPIDEV_DISPLAY(0), true);

  /* Now make sure that the SPI bus is configured for the st7301 (it
   * might have gotten configured for a different device while unlocked)
   */

  SPI_SETMODE(spi, CONFIG_LCD_ST7301_SPIMODE);
  SPI_SETBITS(spi, bits);
  SPI_SETFREQUENCY(spi, CONFIG_LCD_ST7301_FREQUENCY);
}

/****************************************************************************
 * Name: st7301_deselect
 *
 * Description:
 *   De-select the SPI
 *
 * Input Parameters:
 *   spi  - Reference to the SPI driver structure
 *
 * Returned Value:
 *   None
 *
 * Assumptions:
 *
 ****************************************************************************/

static void st7301_deselect(FAR struct spi_dev_s *spi)
{
  /* De-select st7301 chip and relinquish the SPI bus. */

  SPI_SELECT(spi, SPIDEV_DISPLAY(0), false);
  SPI_LOCK(spi, false);
}

/****************************************************************************
 * Name: st7301_sendcmd
 *
 * Description:
 *   Send a command to the driver.
 *
 ****************************************************************************/

static inline void st7301_sendcmd(FAR struct st7301_dev_s *dev, uint8_t cmd)
{
  st7301_select(dev->spi, 8);
  SPI_CMDDATA(dev->spi, SPIDEV_DISPLAY(0), true);
  SPI_SEND(dev->spi, cmd);
  SPI_CMDDATA(dev->spi, SPIDEV_DISPLAY(0), false);
  st7301_deselect(dev->spi);
}

/****************************************************************************
 * Name: st7301_sleep
 *
 * Description:
 *   Sleep or wake up the driver.
 *
 ****************************************************************************/

static void st7301_sleep(FAR struct st7301_dev_s *dev, bool sleep)
{
  st7301_sendcmd(dev, sleep ? CMD_SLPIN : CMD_SLPOUT);

  up_mdelay(5);

  /* NOTE: after sleep out, we should not call sleep in before 120ms,
   * this is up to the user to enforce
   */
}

/****************************************************************************
 * Name: st7301_lowpower
 *
 * Description:
 *   Set low-power or high-power mode
 *
 ****************************************************************************/

static void st7301_lowpower(FAR struct st7301_dev_s *dev, bool on)
{
  st7301_sendcmd(dev, on ? CMD_LPM : CMD_HPM);
}

/****************************************************************************
 * Name: st7301_fastmode
 *
 * Description:
 *   Configure LCD for either fast or slow mode
 *
 ****************************************************************************/

static void st7301_fastmode(FAR struct st7301_dev_s *dev, bool on)
{
  if (on)
    {
      st7301_lowpower(dev, false);
    }

  /* VCOMH Voltage Setting */

  st7301_sendcmd(dev, CMD_VCOMCTRL);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, on ? 0x15 : 0x1f);
  st7301_deselect(dev->spi);

  /* Source Voltage Select */

  st7301_sendcmd(dev, CMD_VSHLSEL);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, on ? 0x1 : 0x3);
  st7301_deselect(dev->spi);

  //up_mdelay(40);

  if (!on)
    {
      st7301_lowpower(dev, true);
    }
}

/****************************************************************************
 * Name: st7301_display
 *
 * Description:
 *   Turn on or off the display.
 *
 ****************************************************************************/

static void st7301_display(FAR struct st7301_dev_s *dev, bool on)
{
  st7301_sendcmd(dev, on ? CMD_DISPON : CMD_DISPOFF);
}

/****************************************************************************
 * Name: st7301_setarea
 *
 * Description:
 *   Set the rectangular area for an upcomming read or write from RAM.
 *
 ****************************************************************************/

static void st7301_setarea(FAR struct st7301_dev_s *dev,
                           uint16_t x0, uint16_t y0,
                           uint16_t x1, uint16_t y1)
{
  /* The LCD requires data in chunks of four */

  DEBUGASSERT(VALID_COL_RANGE(x0,x1));

  /* TODO: the above is only true for four-byte write mode, for three
   * byte mode the check should be different
   */

  /* Set column address */

  st7301_sendcmd(dev, CMD_CASET);
  st7301_select(dev->spi, 16);
  SPI_SEND(dev->spi, 8 + (x0 >> 2));
  SPI_SEND(dev->spi, 8 + (x1 >> 2));
  st7301_deselect(dev->spi);

  /* Set row address */

  st7301_sendcmd(dev, CMD_RASET);
  st7301_select(dev->spi, 16);
  SPI_SEND(dev->spi, y0);
  SPI_SEND(dev->spi, y1);
  st7301_deselect(dev->spi);
}

/****************************************************************************
 * Name: st7301_wrram
 *
 * Description:
 *   Write to the driver's RAM.
 *
 ****************************************************************************/

static void st7301_wrram(FAR struct st7301_dev_s *dev,
                         FAR const uint16_t *buff, size_t size)
{
  st7301_sendcmd(dev, CMD_RAMWR);

  st7301_select(dev->spi, 8);
  SPI_SNDBLOCK(dev->spi, buff, size);
  st7301_deselect(dev->spi);
}

/****************************************************************************
 * Name:  st7301_putrun
 *
 * Description:
 *   This method can be used to write a partial raster line to the LCD:
 *
 *   row     - Starting row to write to (range: 0 <= row < yres)
 *   col     - Starting column to write to (range: 0 <= col <= xres-npixels)
 *   buffer  - The buffer containing the run to be written to the LCD
 *   npixels - The number of pixels to write to the LCD
 *             (range: 0 < npixels <= xres-col)
 *
 ****************************************************************************/

static int st7301_putrun(fb_coord_t row, fb_coord_t col,
                         FAR const uint8_t *buffer, size_t npixels)
{
  FAR struct st7301_dev_s *priv = &g_lcddev;
  FAR const uint16_t *src = (FAR const uint16_t *)buffer;
  size_t col_end = col + npixels - 1;

  ginfo("row: %d col: %d npixels: %d\n", row, col, npixels);

  if (!VALID_COL_RANGE(col, col_end))
    {
      gerr("Column range needs to be aligned to four bytes");
      return -EINVAL;
    }

  st7301_setarea(priv, col, row, col_end, row);
  st7301_wrram(priv, src, npixels);

  return OK;
}

/****************************************************************************
 * Name: st7301_putarea
 *
 * Description:
 *   This method can be used to write a rectangular area to the LCD:
 *
 *   row_start - Starting row to write to (range: 0 <= row < yres)
 *   row_end   - Ending row to write to (range: row_start <= row < yres)
 *   col_start - Starting column to write to (range: 0 <= col <= xres)
 *   col_end   - Ending column to write to
 *               (range: col_start <= col_end < xres)
 *   buffer    - The buffer containing the area to be written to the LCD
 *
 *   NOTE: this operation may not be supported by the device, in which case
 *   the callback pointer will be NULL. In that case, putrun() should be
 *   used.
 */

static int st7301_putarea(fb_coord_t row_start, fb_coord_t row_end,
                          fb_coord_t col_start, fb_coord_t col_end,
                          FAR const uint8_t *buffer)
{
  FAR struct st7301_dev_s *priv = &g_lcddev;
  FAR const uint16_t *src = (FAR const uint16_t *)buffer;
  size_t npixels = (row_end - row_start + 1) *
                   (col_end - col_start + 1);

  if (!VALID_COL_RANGE(col_start, col_end))
    {
      gerr("Column range needs to be aligned to four bytes");
      return -EINVAL;
    }

  lcdinfo("row_start: %d row_end: %d col_start: %d col_end: %d\n",
          row_start, row_end, col_start, col_end);

  st7301_setarea(priv, col_start, row_start, col_end, row_end);
  st7301_wrram(priv, src, npixels);

  return OK;
}

/****************************************************************************
 * Name:  st7301_getvideoinfo
 *
 * Description:
 *   Get information about the LCD video controller configuration.
 *
 ****************************************************************************/

static int st7301_getvideoinfo(FAR struct lcd_dev_s *dev,
                               FAR struct fb_videoinfo_s *vinfo)
{
  DEBUGASSERT(dev && vinfo);
  lcdinfo("fmt: %d xres: %d yres: %d nplanes: 1\n",
          ST7301_COLORFMT, ST7301_XRES, ST7301_YRES);

  vinfo->fmt     = ST7301_COLORFMT;    /* Color format: RGB16-565: RRRR RGGG GGGB BBBB */
  vinfo->xres    = ST7301_XRES;        /* Horizontal resolution in pixel columns */
  vinfo->yres    = ST7301_YRES;        /* Vertical resolution in pixel rows */
  vinfo->nplanes = 1;                  /* Number of color planes supported */
  return OK;
}

/****************************************************************************
 * Name:  st7301_getplaneinfo
 *
 * Description:
 *   Get information about the configuration of each LCD color plane.
 *
 ****************************************************************************/

static int st7301_getplaneinfo(FAR struct lcd_dev_s *dev,
                               unsigned int planeno,
                               FAR struct lcd_planeinfo_s *pinfo)
{
  FAR struct st7301_dev_s *priv = (FAR struct st7301_dev_s *)dev;

  DEBUGASSERT(dev && pinfo && planeno == 0);
  lcdinfo("planeno: %d bpp: %d\n", planeno, ST7301_BPP);

  pinfo->putrun  = st7301_putrun;                  /* Put a run into LCD memory */
  pinfo->putarea = st7301_putarea;
  pinfo->buffer  = NULL;
  pinfo->bpp     = priv->bpp;                      /* Bits-per-pixel */
  return OK;
}

/****************************************************************************
 * Name:  st7301_getpower
 ****************************************************************************/

static int st7301_getpower(FAR struct lcd_dev_s *dev)
{
  FAR struct st7301_dev_s *priv = (FAR struct st7301_dev_s *)dev;

  lcdinfo("power: %d\n", priv->power);
  return priv->power;
}

/****************************************************************************
 * Name:  st7301_setpower
 ****************************************************************************/

static int st7301_setpower(FAR struct lcd_dev_s *dev, int power)
{
  FAR struct st7301_dev_s *priv = (FAR struct st7301_dev_s *)dev;

  lcdinfo("power: %d\n", power);
  DEBUGASSERT((unsigned)power <= CONFIG_LCD_MAXPOWER);

  if (priv->power == power)
    {
      return OK;
    }

  /* Set new power level */

  if (power > 0)
    {
      if (priv->power == 0)
        {
          /* Exit sleep mode */

          st7301_sleep(priv, false);
        }

      if (power == 1)
        {
          st7301_fastmode(priv, false);
        }
      else
        {
          st7301_fastmode(priv, true);
        }

      /* Save the power */

      priv->power = power;
    }
  else if (power == 0)
    {
      /* Set to slow mode first */

      if (priv->power == 2)
        {
          st7301_fastmode(priv, false);
        }

      /* Enter sleep mode */

      st7301_sleep(priv, true);

      /* Save the power */

      priv->power = 0;
    }

  return OK;
}

/****************************************************************************
 * Name:  st7301_getcontrast
 *
 * Description:
 *   Get the current contrast setting (0-CONFIG_LCD_MAXCONTRAST).
 *
 ****************************************************************************/

static int st7301_getcontrast(FAR struct lcd_dev_s *dev)
{
  lcdinfo("Not implemented\n");
  return -ENOSYS;
}

/****************************************************************************
 * Name:  st7301_setcontrast
 *
 * Description:
 *   Set LCD panel contrast (0-CONFIG_LCD_MAXCONTRAST).
 *
 ****************************************************************************/

static int st7301_setcontrast(FAR struct lcd_dev_s *dev,
                              unsigned int contrast)
{
  lcdinfo("contrast: %d\n", contrast);
  return -ENOSYS;
}

/****************************************************************************
 * Name:  st7301_init
 *
 * Description:
 *   Initialization sequence for LCD
 *
 ****************************************************************************/

static void st7301_init(FAR struct st7301_dev_s *dev)
{
  /* NVM Load Enable */

  st7301_sendcmd(dev, CMD_NVMLOADEN);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x02);
  st7301_deselect(dev->spi);

  /* NVM Load Control */

  st7301_sendcmd(dev, CMD_NVMLOADCTRL);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x68);
  st7301_deselect(dev->spi);

  /* Booster Enable */

  st7301_sendcmd(dev, CMD_BSTEN);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x01);
  st7301_deselect(dev->spi);

  /* Propietary command */

  st7301_sendcmd(dev, 0xe8);
  st7301_select(dev->spi, 16);
  SPI_SEND(dev->spi, 0x55);
  SPI_SEND(dev->spi, 0x06);
  st7301_deselect(dev->spi);

  /* Gate Voltage Control */

  st7301_sendcmd(dev, CMD_GCTRL);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0xe5);
  st7301_deselect(dev->spi);

  /* Frame Rate Control */

  st7301_sendcmd(dev, CMD_FRCTRL);
  st7301_select(dev->spi, 16);
  SPI_SEND(dev->spi, 0x1); /* High frame-rate: 51/32Hz according to OSC */
  SPI_SEND(dev->spi, 0x2); /* Low frame-rate: 1Hz */
  //SPI_SEND(dev->spi, 0x5); /* Low frame-rate: 8Hz */
  st7301_deselect(dev->spi);

  /* Update Period Gate EQ Control */

  st7301_sendcmd(dev, CMD_GATEUPDEQ);
  st7301_select(dev->spi, 80);
  SPI_SEND(dev->spi, 0xc5);
  SPI_SEND(dev->spi, 0x77);
  SPI_SEND(dev->spi, 0xf1);
  SPI_SEND(dev->spi, 0xff);
  SPI_SEND(dev->spi, 0xff);
  SPI_SEND(dev->spi, 0x4f);
  SPI_SEND(dev->spi, 0xf1);
  SPI_SEND(dev->spi, 0xff);
  SPI_SEND(dev->spi, 0xff);
  SPI_SEND(dev->spi, 0x4f);
  st7301_deselect(dev->spi);

  /* Source EQ Enable */

  st7301_sendcmd(dev, CMD_SOUEQ);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0);
  st7301_deselect(dev->spi);

  /* Gate Line Setting */

  st7301_sendcmd(dev, CMD_GATESET);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x59);
  st7301_deselect(dev->spi);

  /* Sleep Out */

  st7301_sleep(dev, false);

  /* OSC Setting */

  st7301_sendcmd(dev, CMD_OSCSET);
  st7301_select(dev->spi, 16);
  SPI_SEND(dev->spi, 0x80); /* Enable OSC, Max 51Hz */
  //SPI_SEND(dev->spi, 0x80 | 3); /* Enable OSC, Max 32Hz */
  SPI_SEND(dev->spi, 0xe9); /* Default value */
  st7301_deselect(dev->spi);

  /* Source Voltage Select */

  st7301_sendcmd(dev, CMD_VSHLSEL);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x1);
  st7301_deselect(dev->spi);

  /* VCOMH Voltage Setting */

  st7301_sendcmd(dev, CMD_VCOMCTRL);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x15);
  st7301_deselect(dev->spi);

  /* Memory Data Access Control */

  st7301_sendcmd(dev, CMD_MADCTL);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, /*MADCTL_MX_RTL | MADCTL_DO_RTL*/ 0x48);
  st7301_deselect(dev->spi);

  // TODO: with above we can support rotation

  /* Data Format Select:
   * Four writes (6 bit per write), Up-down ON, 16/64 color
   * This results in 222 RGB (in MSB part of byte)
   */

  st7301_sendcmd(dev, CMD_DTFORM);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, DTFORM_XDE_ON);
  st7301_deselect(dev->spi);

  /* Gate Power Save */

  st7301_sendcmd(dev, CMD_GPS);
  st7301_select(dev->spi, 32);
  SPI_SEND(dev->spi, 0x20);
  SPI_SEND(dev->spi, 0x4);
  SPI_SEND(dev->spi, 0x80);
  SPI_SEND(dev->spi, 0xfa);
  st7301_deselect(dev->spi);

  /* Propietary command */

  st7301_sendcmd(dev, 0xb5);
  st7301_select(dev->spi, 16);
  SPI_SEND(dev->spi, 0x2);
  SPI_SEND(dev->spi, 0x0);
  st7301_deselect(dev->spi);

  /* Source Setting */

  st7301_sendcmd(dev, CMD_SOCSET);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x23 | (3 << 6));  /* Clear RAM ON */
  st7301_deselect(dev->spi);

  st7301_sendcmd(dev, CMD_SOCSET);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x23);     /* Clear RAM OFF */
  st7301_deselect(dev->spi);

  /* Panel Setting */

  st7301_sendcmd(dev, CMD_PNLSET);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x8);
  st7301_deselect(dev->spi);

  /* Set initial area */

  st7301_setarea(dev, 0, 0, ST7301_XRES - 1, ST7301_YRES - 1);

  /* Tearing Effect Line On */

  st7301_sendcmd(dev, CMD_TEON);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0);
  st7301_deselect(dev->spi);

  /* Auto Power Control */

  st7301_sendcmd(dev, CMD_AUTOPWRCTRL);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x1f);
  st7301_deselect(dev->spi);

  /* High Power Mode ON */

  st7301_sendcmd(dev, CMD_HPM);

  /* High Voltage Mode */

  st7301_sendcmd(dev, CMD_HIGHVMODE);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x1);
  st7301_deselect(dev->spi);

  /* Propietary command */

  st7301_sendcmd(dev, 0xc4);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0xb1);
  st7301_deselect(dev->spi);

  /* High Voltage Mode */

  st7301_sendcmd(dev, CMD_HIGHVMODE);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x3);
  st7301_deselect(dev->spi);

  /* Propietary command */

  st7301_sendcmd(dev, 0xe3);
  st7301_select(dev->spi, 8);
  SPI_SEND(dev->spi, 0x2);
  st7301_deselect(dev->spi);

  /* Write Memory Continue */

  st7301_sendcmd(dev, CMD_WRMEMC);
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name:  st7301_initialize
 *
 * Description:
 *   Initialize the ST7301 video hardware.  The initial state of the
 *   LCD is fully initialized, display memory cleared, and the LCD ready
 *   to use, but with the power setting at 0 (full off == sleep mode).
 *
 * Returned Value:
 *
 *   On success, this function returns a reference to the LCD object for
 *   the specified LCD.  NULL is returned on any failure.
 *
 ****************************************************************************/

FAR struct lcd_dev_s *st7301_lcdinitialize(FAR struct spi_dev_s *spi)
{
  FAR struct st7301_dev_s *priv = &g_lcddev;

  /* Initialize the driver data structure */

  priv->dev.getvideoinfo = st7301_getvideoinfo;
  priv->dev.getplaneinfo = st7301_getplaneinfo;
  priv->dev.getpower     = st7301_getpower;
  priv->dev.setpower     = st7301_setpower;
  priv->dev.getcontrast  = st7301_getcontrast;
  priv->dev.setcontrast  = st7301_setcontrast;
  priv->spi              = spi;

  /* Init the hardware and clear the display */

  st7301_init(priv);

  up_mdelay(10);

  /* Display ON */

  st7301_display(priv, true);

  /* Fast Mode */

  st7301_setpower(&priv->dev, 2);

  return &priv->dev;
}

