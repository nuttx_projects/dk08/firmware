#!/bin/bash

openocd -f interface/cmsis-dap.cfg -f target/nrf52.cfg -c "set reset_config connect_assert_srst; program nuttx/nuttx.hex verify reset; shutdown"
